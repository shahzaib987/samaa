package samaaapp3_new.samaatv.com.newappsamaatv;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    MediaPlayer mp;
    int pos;
    View custom_view;
    // JSON Ticker Just-In XML
  //  String URL = "";

    //SwipeRefreshLayout swipeLayout;
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static TextView ticker_head;
    public static LinearLayout frameticker;
    public static PublisherAdView mAdView;
    int ITEMS_COUNT = 10;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);



      /*  try {
            File httpCacheDir = new File(this.getCacheDir(), "http");
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            HttpResponseCache.install(httpCacheDir, httpCacheSize);
        } catch (IOException e) {
            Log.i("HTTP Response", "HTTP response cache installation failed:" + e);
        }*/

        //SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(Splash.this);

        //boolean checked2 = sharedPref.getBoolean("startup_sound", true);

        boolean checked2 = true;

        if (checked2) {
            mp = new MediaPlayer();
            mp = MediaPlayer.create(getBaseContext(), R.raw.samaa_notify);
           // mp.start(); //Starts SamaaTV Splash Sound
        }
        custom_view = (View) findViewById(R.id.customview);
        mAdView = (PublisherAdView) findViewById(R.id.ad_view);
       // toolbar.setLogo(R.drawable.logo_samaatv);

       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();

        // Start loading the ad in the background.
        mAdView.loadAd(adRequest);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(ITEMS_COUNT-1);
        setupViewPager(viewPager);

        frameticker = (LinearLayout) findViewById(R.id.linearlayout2);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        ticker_head = (TextView) findViewById(R.id.ticker);

        //For Animating News Ticker Starts Here

     /*   int textWidth = ticker_head.getMeasuredWidth();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        Animation animationToLeft = new TranslateAnimation(ticker_head.length(), -textWidth, 0, 0);

        animationToLeft.setDuration(8000);
        animationToLeft.setRepeatMode(Animation.RESTART);
        animationToLeft.setRepeatCount(Animation.INFINITE);

        ticker_head.setAnimation(animationToLeft);
        ticker_head.setVisibility(View.GONE);
        */
        //For Animating News Ticker Ends Here

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
              pos = tab.getPosition();
              viewPager.setCurrentItem(pos);

                if(pos==0)
                {
                    mAdView.setVisibility(View.GONE);
                    frameticker.setVisibility(View.GONE);
                    custom_view.setVisibility(View.GONE);
                }
                else if(pos==1)
                {
                    mAdView.setVisibility(View.VISIBLE);
                    frameticker.setVisibility(View.VISIBLE);
                    custom_view.setVisibility(View.VISIBLE);

                }
                else
                {
                    mAdView.setVisibility(View.GONE);
                    frameticker.setVisibility(View.GONE);
                    custom_view.setVisibility(View.GONE);
                }


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        viewPager.getChildAt(0).setBackgroundColor(getResources().getColor(R.color.red));
         /*TabLayout.Tab tab = tabLayout.getTabAt(0);

            if (tab != null) {

              tab.setIcon(R.drawable.samaa_logo);
              tab.setText("");

            }*/

     //For Future reference to get particular tab--------------

        //cast the selected tablayout to viewgroup
       /* ViewGroup vg = (ViewGroup) tabs.getChildAt(0);
        //count how many tabs in tablayout
        int tabsCount = vg.getChildCount();
        //iterative each tab
        for (int j = 0; j < tabsCount; j++) {
            //Get all element in each tabs and cast to viewgroup
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            //count the element in each tabs
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                // cast to View
                View tabViewChild = vgTab.getChildAt(i);
            }*/

     // Reference ends here for finding particular tab---------------------


        //For setting red color for first tab !

        final ViewGroup root = (ViewGroup) tabLayout.getChildAt(0);
        final View tab = root.getChildAt(0);
        tab.setBackgroundColor(getResources().getColor(R.color.heading2_black));


        /*if(tabLayout.getTabAt(pos).getCustomView().setSelected(true))
        {
            MainActivity.mAdView.setVisibility(View.VISIBLE);
        }
        else
        {
            MainActivity.mAdView.setVisibility(View.GONE);
        }*/
        // ends here red color of first tab

        //ticker_head.setText("SamaaTV News Ticker....");
        //Element line = (Element) title_list.item(0);

        //titlelist[temp] = getCharacterDataFromElement(line);
        //ticker.setVisibility(View.GONE);
        //ticker.setText(titlelist[temp].toString());
        //ticker.setMovementMethod(new ScrollingMovementMethod());

        /*int textWidth = ticker_head.getMeasuredWidth();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        //int height = displaymetrics.heightPixels;
        int screenWidth = displaymetrics.widthPixels;
        //	textWidth=ticker.length();
        // Get the screen width
        //Point size = new Point();
					*//*Display display = getWindowManager().getDefaultDisplay();
					int screenWidth = display.getWidth();  // deprecated*//*
        //int height = display.getHeight();
        //Point outSize = new Point();
        //getWindowManager().getDefaultDisplay().getSize(outSize);
        //	screenWidth = outSize.x;
        //screenWidth = size.x;


        Animation animationToLeft = new TranslateAnimation(ticker_head.length(), -textWidth, 0, 0);

				*//*	ViewGroup.LayoutParams params = ticker.getLayoutParams();
					params.width = getResources().getDimensionPixelSize(R.dimen.text_view_width);
					ticker.setLayoutParams(params);*//*

        animationToLeft.setDuration(12000);
        animationToLeft.setRepeatMode(Animation.RESTART);
        animationToLeft.setRepeatCount(Animation.INFINITE);


        ticker_head.setAnimation(animationToLeft);
        ticker_head.setVisibility(View.GONE);
*/

    }

    public void switchContent(int id, Fragment fragment) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(id, fragment, fragment.toString());
      //  getFragmentManager().popBackStackImmediate();
        ft.addToBackStack(null);
       // ft.commitAllowingStateLoss();
        ft.commit();
    }


    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new WatchLiveEnglish_web(), "Live");
        adapter.addFrag(new TrendingNow(), "Trending");
        adapter.addFrag(new PakistanNews(), "Pakistan");
        adapter.addFrag(new WorldNews(), "Global");
        adapter.addFrag(new BusinessNews(), "Economy");
        adapter.addFrag(new SportsNews(), "Sports");
        adapter.addFrag(new EntertainmentNews(), "Entertainment");
        adapter.addFrag(new EditorsChoice(), "Editor's Choice");
        adapter.addFrag(new TvShows(), "TV Shows");
        adapter.addFrag(new BlogFeatures(), "Blogs");




//        tabLayout.getTabAt(0).setIcon(R.drawable.samaa_logo);



        // More Categories- Health, Lifestyle, Sci tech, social buzz, weird

      /*  adapter.addFrag(new EightFragment(), "EIGHT");
        adapter.addFrag(new NineFragment(), "NINE");
        adapter.addFrag(new TenFragment(), "TEN");*/

         viewPager.setAdapter(adapter);
        //viewPager.setBackgroundColor();
       // viewPager.getCurrentItem().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
         //viewPager.setBackgroundColor(Color.RED);

         viewPager.setCurrentItem(1);


        //For opening Trending now screen firstly !
      //  tabLayout.getTabAt(0).setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
      /*  HttpResponseCache cache = HttpResponseCache.getInstalled();
        if (cache != null) {
            cache.flush();
        }*/
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();

        }
    }


    @Override
    public void onPause() {
        // Pause the PublisherAdView.
        if (mAdView != null){
            mAdView.pause();
             }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Destroy the PublisherAdView.
        if (mAdView != null) {
            mAdView.destroy();
          }
        super.onDestroy();
    }

   /* public void onRefresh() {
        // TODO Auto-generated method stub
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {

            }
        }, 5000);*/
       /* new AsyncLoadXMLFeed().execute();
        swipeLayout.setRefreshing(false);*/


}
