package samaaapp3_new.samaatv.com.newappsamaatv;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;

import org.json.JSONArray;
import org.json.JSONException;

//import me.relex.circleindicator.CircleIndicator;


public class Detail_Activity extends AppCompatActivity {
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
   // private static final int NUM_PAGES = 5;

    static PublisherInterstitialAd mPublisherInterstitialAd;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    public ViewPager mPager;
    JSONArray array;
    String jsonArray;
    int pos;
    String cate;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(R.layout.app_bar_detail);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.toolbar_layoutcolor)));

        // Create the InterstitialAd and set the adUnitId.
        mPublisherInterstitialAd = new PublisherInterstitialAd(this);
        // Defined in res/values/strings.xml
        mPublisherInterstitialAd.setAdUnitId(getString(R.string.interstitial_unit_id));

        PublisherAdRequest.Builder publisherAdRequestBuilder = new PublisherAdRequest.Builder();
        mPublisherInterstitialAd.loadAd(publisherAdRequestBuilder.build());
        //setTitle("Detail News");
        Intent intent = getIntent();
        jsonArray = intent.getStringExtra("jsonArray");
        try {
            array = new JSONArray(jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        pos = intent.getExtras().getInt("pos");

        Log.e("","Position Activity is :"+pos);

       //feed = (RSSFeed) getIntent().getExtras().get("feed");

       // cate= getIntent().getExtras().getString("category");


        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
       // CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);

        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(pos);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

           /*if((position % 4) == 0 && mPublisherInterstitialAd != null && mPublisherInterstitialAd.isLoaded())

             {
                 mPublisherInterstitialAd.show();
             }*/


            }

            @Override
            public void onPageSelected(int position) {
               /* Log.d("test", "position = " + position);
                pos=position;
                Log.d("test2", "position2 = " + pos);
              //  pos = position;*/
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
      //  indicator.setViewPager(mPager);
      //  mPagerAdapter.registerDataSetObserver(indicator.getDataSetObserver());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class MyPagerAdapter extends FragmentStatePagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            DetailFragment frag = new DetailFragment();
            //pos = position;
            Bundle bundle = new Bundle();
            bundle.putString("jsonArray", jsonArray);
            bundle.putInt("pos", position);
          //  bundle.putString("category", cate);

            //frag.set(position, fragment);
            //bundle.putString(KEY_VIDEO, videourl);
            frag.setArguments(bundle);

            return frag;

        }

        @Override
        public int getCount() {
            return array.length();
        }
    }
}