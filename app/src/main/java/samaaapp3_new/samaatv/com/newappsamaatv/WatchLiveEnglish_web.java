package samaaapp3_new.samaatv.com.newappsamaatv;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import com.google.ads.AdRequest;
//import com.google.ads.AdSize;
//import com.google.ads.AdView;
//import android.widget.Toast;


public class WatchLiveEnglish_web extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
	private TabLayout tabLayout;
	SwipeRefreshLayout swipeLayout;
	private PublisherAdView mAdView;
	public WatchLiveEnglish_web() {
		// Required empty public constructor
	}
	private String TAG = WatchLiveEnglish_web.class.getSimpleName();
	private RecyclerView mRecyclerView;
	private RecyclerView.Adapter pop_videos_adapter;
	JSONArray pop_video;
	static PublisherInterstitialAd mPublisherInterstitialAd;
	int currentapiversion= Build.VERSION.SDK_INT;
	//String currentapiversion= Build.VERSION_CODES;

	//URL For Popular Videos
	private static final String feedURL ="http://www.samaa.tv/videos/jfeedmostwatchedprograms/";

	private VideoEnabledWebView webView;
	private VideoEnabledWebChromeClient webChromeClient;


	private final static String URL = "http://app.samaa.tv/lft/apptest.php"; //For medium_rectangle advertisement
			//"http://www.samaa.tv/live/samaaapptest.php"; previous email
	private final static String USERNAME = "samaaapp";
	private final static String PASSWORD = "samaaapp786";
	private final static String HOST = "http://www.samaa.tv";
	private final static String REALM = "Users Only";


	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Connection detector class
	ConnectionDetector cd;

	//private WebView webViewbro;
	ProgressBar progressBar;

	// flag for Internet connection Connection Fast status
	Boolean isConnectedFast = false;

	// internet speed class
	InternetSpeed is;


	Button buttonPlay;
	Button buttonStop;

	//private AudioStreamActivity musicSrv;
	private Intent playIntent;
	//binding
	private boolean musicBound = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Fragment locked in landscape screen orientation
		//getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {


		// Inflate the layout for this fragment
		//getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.ramzan, container, false);

		//getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		cd = new ConnectionDetector(getActivity());
		/*getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);*/
		mAdView = (PublisherAdView) rootView.findViewById(R.id.ad_view1);

		if(feedURL!=null){

			PublisherAdRequest adRequest = new PublisherAdRequest.Builder().setContentUrl(feedURL).build();

			// Start loading the ad in the background.
			mAdView.loadAd(adRequest);


		}
		else
		{


			PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();

			// Start loading the ad in the background.
			mAdView.loadAd(adRequest);


		}

		isConnectedFast = is.isConnectedFast(getActivity());
// Create the InterstitialAd and set the adUnitId.
		mPublisherInterstitialAd = new PublisherInterstitialAd(getActivity());
		// Defined in res/values/strings.xml
		mPublisherInterstitialAd.setAdUnitId(getString(R.string.interstitial_unit_id));

		PublisherAdRequest.Builder publisherAdRequestBuilder = new PublisherAdRequest.Builder();
		mPublisherInterstitialAd.loadAd(publisherAdRequestBuilder.build());


		if (currentapiversion <= 15) {

			if (mPublisherInterstitialAd != null && mPublisherInterstitialAd.isLoaded()) {
				mPublisherInterstitialAd.show();
			}
			else {


				//Toast.makeText(this, "Ad did not load", Toast.LENGTH_SHORT).show();
				PublisherAdRequest publisherAdRequest = new PublisherAdRequest.Builder().build();
				mPublisherInterstitialAd.loadAd(publisherAdRequest);}

			String videoUrl = "rtsp://38.96.148.99:1935/live/bb";
			Intent i = new Intent(Intent.ACTION_VIEW);
			i.setData(Uri.parse(videoUrl));
			getActivity().finish();
			startActivity(i);
			//super.onBackPressed();
		}


		swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
		swipeLayout.setOnRefreshListener(this);

		swipeLayout.setColorSchemeColors(R.color.toolbar_layoutcolor,
				R.color.heading1_black,
				R.color.heading2_black,
				R.color.blue);



		webView = (VideoEnabledWebView) rootView.findViewById(R.id.webView);



		/*if(currentapiversion >=21){
		webView.setNestedScrollingEnabled(false);
		}*/

		/*webView.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return (event.getAction() == MotionEvent.ACTION_MOVE);
			}
		});*/

		mRecyclerView = (RecyclerView) rootView.findViewById(R.id.pop_videos);

		LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
				LinearLayoutManager.VERTICAL, false);

		//RecyclerView for vertical (Tiles) layout
		mRecyclerView.setLayoutManager(layoutManager);
		mRecyclerView.setNestedScrollingEnabled(false);
		mRecyclerView.setHasFixedSize(true);

				// Initialize the VideoEnabledWebChromeClient and set event handlers
		View nonVideoLayout = rootView.findViewById(R.id.nonVideoLayout); // Your own view, read class comments
		ViewGroup videoLayout = (ViewGroup) rootView.findViewById(R.id.videoLayout); // Your own view, read class comments
		videoLayout.setVisibility(View.GONE);
		//noinspection all
		View loadingView = getActivity().getLayoutInflater().inflate(R.layout.view_loading_video, null); // Your own view, read class comments
		webChromeClient = new VideoEnabledWebChromeClient(nonVideoLayout, videoLayout, loadingView, webView, mRecyclerView) // See all available constructors...
		{
			// Subscribe to standard events, such as onProgressChanged()...
			@Override
			public void onProgressChanged(WebView view, int progress) {
				// Your code...
			}
		};
	webChromeClient.setOnToggledFullscreen(new VideoEnabledWebChromeClient.ToggledFullscreenCallback() {
			@Override
			public void toggledFullscreen(boolean fullscreen) {
				// Your code to handle the full-screen change, for example showing and hiding the title bar. Example:
				if (fullscreen) {
					WindowManager.LayoutParams attrs = getActivity().getWindow().getAttributes();
					attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
					attrs.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
					getActivity().getWindow().setAttributes(attrs);
					((AppCompatActivity) getActivity()).getSupportActionBar().hide();
					MainActivity.tabLayout.setVisibility(View.GONE);
					MainActivity.frameticker.setVisibility(View.GONE);
					getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
					//tabLayout.
					if (Build.VERSION.SDK_INT >= 14) {
						//noinspection all
						getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
					}
				} else {
					WindowManager.LayoutParams attrs = getActivity().getWindow().getAttributes();
					attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
					attrs.flags &= ~WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
					getActivity().getWindow().setAttributes(attrs);
					((AppCompatActivity) getActivity()).getSupportActionBar().show();
					MainActivity.tabLayout.setVisibility(View.VISIBLE);
					MainActivity.frameticker.setVisibility(View.VISIBLE);
					getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

					//tabLayout.setVisibility(View.VISIBLE);
					if (Build.VERSION.SDK_INT >= 14) {
						//noinspection all
						getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
					}
				}

			}
		});


		// Gets the ad view defined in layout/ad_fragment.xml with ad unit ID set in
		// values/strings.xml.
/*		mAdView = (PublisherAdView) findViewById(R.id.ad_view);*/

		// Create an ad request. Check logcat output for the hashed device ID to
		// get test ads on a physical device. e.g.
		// "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
/*
		PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();

		// Start loading the ad in the background.
		mAdView.loadAd(adRequest);
*/


		//setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);


		//String url = "http://ramzan.samaa.tv/";
		//WebView ourBrow=(WebView)findViewById(R.id.webView);
		//ourBrow.getSettings().setJavaScriptEnabled(true);
		//ourBrow.getSettings().setLoadWithOverviewMode(true);
		//ourBrow.getSettings().setLoadsImagesAutomatically(true);
		//ourBrow.getSettings().setUseWideViewPort(true);
		//ourBrow.setWebViewClient(new ourViewClient());
		//ourBrow.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		//ourBrow.loadUrl("http://ramzan.samaa.tv");

		//String customHtml = "<html><body><h2>Greetings from JavaCodeGeeks</h2></body></html>";
		//webView.loadData(customHtml, "text/html", "UTF-8");


		//button declarations
		/*Button btn_back = (Button) findViewById(R.id.btn_back);
		Button btn_refresh = (Button) findViewById(R.id.btn_refresh);
		Button btn_LiveVideo = (Button) findViewById(R.id.btn_video);
		Button btn_LiveAudio = (Button) findViewById(R.id.btn_audio);
		Button btn_home = (Button) findViewById(R.id.btn_home);
		Button btn_TvShows = (Button) findViewById(R.id.btn_tvshowss);*/
		progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);


		//webViewbro = (WebView) findViewById(R.id.webView);
		webView.setWebViewClient(new ourViewClient(getActivity(), webView));
		webView.setWebChromeClient(webChromeClient);


		//webView.getSettings().setDomStorageEnabled(true);
		webView.getSettings().setJavaScriptEnabled(true);
		/*webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);*/

		/*webViewvdo.getSettings().setJavaScriptEnabled(true);
		webViewvdo.getSettings().setLoadWithOverviewMode(true);
		webViewvdo.getSettings().setLoadsImagesAutomatically(true);
		webViewvdo.getSettings().setUseWideViewPort(true);
		webViewvdo.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		webViewvdo.getSettings().setBuiltInZoomControls(true);
		webViewvdo.setVerticalScrollBarEnabled(true);
		webViewvdo.setHorizontalScrollbarOverlay(false);*/
		webView.setHttpAuthUsernamePassword(HOST, REALM, USERNAME, PASSWORD);

	//	isInternetPresent = cd.isConnectingToInternet();

		try {
			if(isConnectedFast) {
				webView.loadUrl(URL);
				new AsyncLoadXMLFeed().execute();
			}
	       }
		catch(Exception e){
			Log.e(TAG, "GetContactAsyncError: " + e.getMessage());
		}


		return rootView;

	}


	private class AsyncLoadXMLFeed extends AsyncTask<Void, Void, Void> {

		ProgressDialog pDialog;


		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog = new ProgressDialog(getActivity());
			pDialog.setMessage("Loading...");
			pDialog.setCanceledOnTouchOutside(false);
			pDialog.show();

		}

		@Override
		protected Void doInBackground(Void... params) {

			ServiceHandler sh = new ServiceHandler();

			String jsonStr = sh.makeServiceCall(feedURL, ServiceHandler.GET);


			Log.d("Response: ", "> " + jsonStr);

			if (jsonStr != null) {
				try {

					JSONObject jsonObj = new JSONObject(jsonStr);

					// Getting JSON Array node of Most watched news
					pop_video = jsonObj.getJSONArray("Most-Watched-Programs");

				} catch (final JSONException e) {
					Log.e(TAG, "Json parsing error: " + e.getMessage());
//					Toast.makeText(getActivity(), "Json parsing error: " + e.getMessage(), Toast.LENGTH_LONG).show();

				}
			} else {
				Log.e("ServiceHandler", "URL'den veri alınamadı.");
			}

			return null;

		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);



			if (null != pDialog && pDialog.isShowing()) {
				pDialog.dismiss();

				pop_videos_adapter = new RecyclerViewMostWatchAdapter(getActivity(), pop_video);
				mRecyclerView.setAdapter(pop_videos_adapter);
				pop_videos_adapter.notifyDataSetChanged();
			}

		}

	}

	public void showAlertDialog(Context context, String title, String message, Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);

		alertDialog.setCanceledOnTouchOutside(false);

		// Setting alert dialog icon
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

		// Setting OK Button
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
				"OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int in) {

						getActivity().finish();


			}
		});

		// Showing Alert Message
		alertDialog.show();
	}


		/*HttpUriRequest request = new HttpGet(YOUR_URL); // Or HttpPost(), depends on your needs
		String credentials = YOUR_USERNAME + ":" + YOUR_PASSWORD;
		String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
		request.addHeader("Authorization", "Basic " + base64EncodedCredentials);

		HttpClient httpclient = new DefaultHttpClient();
		httpclient.execute(request);*/
// You'll need to handle the e


		/*btn_back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				//webViewbro.canGoBack();
				webViewvdo.stopLoading();
				webViewvdo.loadUrl("");
				webViewvdo.reload();
				webViewvdo = null;
				Intent i = new Intent(getApplicationContext(), english2.class);
				finish();
				startActivity(i);
			}
		});


		btn_refresh.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {


				//	finish();
				webViewvdo.stopLoading();
				webViewvdo.loadUrl("");
				webViewvdo.reload();
				webViewvdo = null;
				finish();
				startActivity(getIntent());


			}
		});

		btn_LiveVideo.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Launching News Feed Screen
				//Intent i = new Intent(getApplicationContext(), VideoStreamActivity.class);
				//startActivity(i);

				*//*String videoUrl = "rtsp://38.96.148.99:1935/live/bb";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(videoUrl));
				startActivity(i);*//*
				finish();
				startActivity(getIntent());


			}
		});

*/
		/*btn_LiveAudio.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Launching News Feed Screen
				Intent i = new Intent(getApplicationContext(), AudioStreamActivity.class);
				startActivity(i);

			}
		});*/

		/*//Audio Activity Starts

		buttonPlay = (Button) findViewById(R.id.btn_audio);
		//mPlayer = english2.mPlayer;
		buttonPlay.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {

				buttonPlay.setVisibility(View.GONE);
				buttonStop.setVisibility(View.VISIBLE);

				musicSrv.play();
				//AudioStreamActivity.mp.start();
				*//*english2.mPlayer.reset();
				english2.mPlayer.start();
*//*
				*//*mPlayer = new MediaPlayer();
				mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
				try {
					mPlayer.setDataSource(url);
				} catch (IllegalArgumentException e) {
					Toast.makeText(getApplicationContext(), "Error Streaming Audio...", Toast.LENGTH_LONG).show();
				} catch (SecurityException e) {
					Toast.makeText(getApplicationContext(), "Error Streaming Audio...", Toast.LENGTH_LONG).show();
				} catch (IllegalStateException e) {
					Toast.makeText(getApplicationContext(), "Error Streaming Audio...", Toast.LENGTH_LONG).show();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					mPlayer.prepare();
				} catch (IllegalStateException e) {
					Toast.makeText(getApplicationContext(), "Error Streaming Audio...", Toast.LENGTH_LONG).show();
				} catch (IOException e) {
					Toast.makeText(getApplicationContext(), "Error Streaming Audio...", Toast.LENGTH_LONG).show();
				}
				mPlayer.start();
				Toast.makeText(getApplicationContext(), "Streaming Audio...", Toast.LENGTH_LONG).show();*//*
				*//*if (!english2.isPlaying){
					english2.isPlaying = true;
					buttonStop.setVisibility(View.VISIBLE);
					buttonPlay.setVisibility(View.GONE);
					mPlayer.stop();
				}*//*

			}
		});



		buttonStop = (Button) findViewById(R.id.btnstop);
		buttonStop.setVisibility(View.GONE);
		buttonStop.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				//

				buttonPlay.setVisibility(View.VISIBLE);
				buttonStop.setVisibility(View.GONE);
				musicSrv.stop();
				musicSrv.reset();
			*//*	english2.mPlayer.stop();
				english2.mPlayer.reset();*//*

				*//*if (english2.isPlaying){
					english2.isPlaying = false;
					buttonStop.setVisibility(View.GONE);
					buttonPlay.setVisibility(View.VISIBLE);
					mPlayer.stop();
				}*//*
			}
		});

		if(AudioStreamActivity.isAudioPlaying()){
			buttonPlay.setVisibility(View.GONE);
			buttonStop.setVisibility(View.VISIBLE);

		}
		else {
			buttonPlay.setVisibility(View.VISIBLE);
			buttonStop.setVisibility(View.GONE);
		}


		btn_home.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Launching News Feed Screen
				Intent i = new Intent(getApplicationContext(), english2.class);
				startActivity(i);

			}
		});


		btn_TvShows.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View view) {
				// Launching News Feed Screen
				Intent i = new Intent(getApplicationContext(), TvPrograms.class);
				startActivity(i);

			}
		});
	}
*/

	/*@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		//check config
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			//linearLayout1.setVisibility(View.GONE); //set visibility of linearLayout as Gone
			Toast.makeText(this, "mode landscape", Toast.LENGTH_SHORT).show();
		}

	}*/

	//connect to the service
	/*private ServiceConnection musicConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			AudioStreamActivity.MusicBinder binder = (AudioStreamActivity.MusicBinder) service;
			//get service
			musicSrv = binder.getService();

			musicSrv.setList(songList);
			musicBound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			musicBound = false;
		}
	};
*/


	/*@Override
	protected void onStop() {
		super.onStop();
		GoogleAnalytics.getInstance(this).reportActivityStop(this);

	}
	//start and bind the service when the activity starts
	@Override
	protected void onStart() {
		super.onStart();
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
		if (playIntent == null) {
			playIntent = new Intent(this, AudioStreamActivity.class);
			bindService(playIntent, musicConnection, Context.BIND_AUTO_CREATE);
			startService(playIntent);
		}
	}*/

	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.my, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**///** Called when leaving the activity *//**//*
	@Override
	public void onPause() {

		super.onPause();
		webView.onPause();
		webView.pauseTimers();
	}

	//**//** Called when returning to the activity *//**//*
	@Override
	public void onResume() {
		super.onResume();
		webView.onResume();
		webView.resumeTimers();

	}

	//**//** Called before the activity is destroyed *//**//*
	@Override
	public void onDestroy() {

		if(webView!=null){
		webView.destroy();
		webView = null;}
		super.onDestroy();
	}


	public class ourViewClient extends WebViewClient {
		private String loginCookie;
		private Context mContext;
		private WebView mWebView;



		@Override
		public boolean shouldOverrideUrlLoading(WebView v, String url)
		{
			/*v.loadUrl(url);
			return true;*/

			if (url != null && url.startsWith("http://")) {
				v.getContext().startActivity(
				new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
				return true;
			}
			else if (url != null && url.startsWith("https://")){
				v.getContext().startActivity(
				new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
				return true;
			}

			else {
				return false;
			}
		}



		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// TODO Auto-generated method stub
			super.onPageStarted(view, url, favicon);


			//Toast.makeText(getApplicationContext(), "Loading...", Toast.LENGTH_LONG).show();
		}
		@Override
		public void onPageFinished(WebView view, String url) {
			// TODO Auto-generated method stub
			super.onPageFinished(view, url);
			CookieManager cookieManager = CookieManager.getInstance();
			cookieManager.setCookie(url, loginCookie);
			progressBar.setVisibility(View.GONE);

		}


		public ourViewClient(Context context, WebView webview) {
			super();

			mContext = context;
			mWebView = webview;
		}

		/*@Override
		public void onReceivedError( WebView view, int errorCode, String description, String failingUrl ) {
			Toast.makeText(view.getContext(), "Error Loading Live Streaming..", Toast.LENGTH_LONG).show();
		}*/

		@SuppressWarnings("deprecation")
		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			// Handle the error
			Toast.makeText(view.getContext(), "Error Loading Live Streaming..", Toast.LENGTH_LONG).show();
		}

		@TargetApi(Build.VERSION_CODES.M)
		@Override
		public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
			// Redirect to deprecated method, so you can use it in all SDK versions
			onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
		}

		@Override
		public void onLoadResource( WebView view, String url ){
			CookieManager cookieManager = CookieManager.getInstance();
			loginCookie = cookieManager.getCookie(url);
		}

		@Override
		public void onReceivedSslError( WebView view, SslErrorHandler handler, SslError error ) {
			super.onReceivedSslError(view, handler, error);

			// this will ignore the Ssl error and will go forward to your site
			handler.proceed();

		}

		@Override
		public void onReceivedHttpAuthRequest( WebView view, final HttpAuthHandler handler, final String host, final String realm ){


			String userName = null;
			String userPass = null;

			if (handler.useHttpAuthUsernamePassword() && view != null) {
				String[] haup = view.getHttpAuthUsernamePassword(host, realm);
				if (haup != null && haup.length == 2) {
					userName = haup[0];
					userPass = haup[1];
				}
			}

			if (userName != null && userPass != null) {
				handler.proceed(userName, userPass);
			}
			else {
				showHttpAuthDialog(handler, host, realm, null, null, null);
			}
		}

		private void showHttpAuthDialog( final HttpAuthHandler handler, final String host, final String realm, final String title, final String name, final String password ) {




			String userName = "samaaapp";

			String userPass = "samaaapp786";

			mWebView.setHttpAuthUsernamePassword(host, realm, name, password);

			handler.proceed(userName, userPass);

		}




	}
	/*@Override
	public void onBackPressed() {

		if(webView.canGoBack()) {
			webView.goBack();
			webView.stopLoading();
			webView.loadUrl("about:blank");
			webView.reload();
			webView = null;
			webView.clearCache(true);

		} else {
			super.getActivity().onBackPressed();
		}*/

	public void onRefresh() {
		// TODO Auto-generated method stub
		new Handler().postDelayed(new Runnable() {
			@Override public void run() {
				webView.loadUrl(URL);
				pop_videos_adapter = new RecyclerViewMostWatchAdapter(getActivity(), pop_video);
				mRecyclerView.setAdapter(pop_videos_adapter);
				pop_videos_adapter.notifyDataSetChanged();
				swipeLayout.setRefreshing(false);
			}
		}, 5000);
       /* new AsyncLoadXMLFeed().execute();
        swipeLayout.setRefreshing(false);*/
	}
}





