package samaaapp3_new.samaatv.com.newappsamaatv;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

//import android.view.View;
//import android.widget.Button;

public class VideoTrend extends Activity {
    int currentapiversion= Build.VERSION.SDK_INT;
    // XML node keys

   /* static final String KEY_TITLE = "title";
    static final String KEY_ARTIST = "artist";
    static final String KEY_DURATION = "duration";*/

    static final String KEY_VIDEO = "videourl";
    ProgressDialog progDailog;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.play_video);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //Button btn=(Button)findViewById(R.id.button1);
        // getting intent data
        Intent in = getIntent();
        // Get XML values from previous intent
        String videourl = in.getStringExtra(KEY_VIDEO);
        /*String cost = in.getStringExtra(KEY_ARTIST);
        String description = in.getStringExtra(KEY_DURATION);*/

        // Displaying all values on the screen
      /*  TextView lblName = (TextView) findViewById(R.id.name_label);
        TextView lblCost = (TextView) findViewById(R.id.cost_label);
        TextView lblDesc = (TextView) findViewById(R.id.description_label);
*/
     /*   lblName.setText(name);
        lblCost.setText(cost);
        lblDesc.setText(description);
*/




        //String a="http://110.93.211.44/abc/trimmed.mp4";

        VideoView video = (VideoView)findViewById(R.id.vdo1);
        //video.setVideoPath(name);
        //video.start();

       /* Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(name));

        startActivity(i);*/


if(videourl!=null) {
    video.setVideoURI(Uri.parse(videourl));
    video.setMediaController(new MediaController(this));
    video.requestFocus();
    video.setFocusableInTouchMode(true);
    video.setZOrderOnTop(true);
    video.requestFocus();
    video.start();
}
else{
    Toast.makeText(VideoTrend.this, "Sorry video is not available", Toast.LENGTH_SHORT).show();
    finish();
}
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);


        /*    // Set an AdListener.
            english2.mPublisherInterstitialAd.setAdListener(new AdListener() {
           *//* @Override
            public void onAdLoaded() {

                // Toast.makeText(Splash.this, "The interstitial is loaded", Toast.LENGTH_SHORT).show();
                //showInterstitial();
            }*//*

                @Override
                public void onAdClosed() {
                    //Proceed to the next Main Activity
                    //Toast.makeText(Splash.this, "Ad is closed", Toast.LENGTH_SHORT).show();
                *//*Intent i = new Intent(getApplicationContext(), ProgramVideos.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);*//*
                    finish();
                    //startActivity(i);
                }
            });*/

        progDailog = ProgressDialog.show(this, "Please wait ...", "Retrieving data ...", true);
        progDailog.setCanceledOnTouchOutside(false);

        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                progDailog.dismiss();
            }
        });

        if (currentapiversion >= 17) {
            video.setOnInfoListener(new MediaPlayer.OnInfoListener() {

                @Override
                public boolean onInfo(MediaPlayer mp, int what, int extra) {
                    switch (what) {
                        case MediaPlayer.MEDIA_INFO_BUFFERING_START:
                            progDailog.show();
                            break;
                        case MediaPlayer.MEDIA_INFO_BUFFERING_END:
                            progDailog.dismiss();
                            break;
                    }
                    return false;
                }
            });
        }
        else
        {

        }

        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
              /*  if (english2.mPublisherInterstitialAd != null && english2.mPublisherInterstitialAd.isLoaded()) {
                    english2.mPublisherInterstitialAd.show();
                } else {


                    //Toast.makeText(this, "Ad did not load", Toast.LENGTH_SHORT).show();
                   *//* PublisherAdRequest publisherAdRequest = new PublisherAdRequest.Builder().build();
                    Splash.mPublisherInterstitialAd.loadAd(publisherAdRequest);*//*
                    finish();
                }*/
            }
        });


        progDailog.setCancelable(true);
        progDailog.setOnCancelListener(new OnCancelListener(){
            @Override
            public void onCancel(DialogInterface dialog){
                progDailog.dismiss();


                //  SendBack Code
                // finish();
            }});

    }

    @Override
    public void onBackPressed() {

      /*  if (english2.mPublisherInterstitialAd != null && english2.mPublisherInterstitialAd.isLoaded()) {
            english2.mPublisherInterstitialAd.show();
        } else {*/
            finish();

            /*//Toast.makeText(this, "Ad did not load", Toast.LENGTH_SHORT).show();
            PublisherAdRequest publisherAdRequest = new PublisherAdRequest.Builder().build();
            english2.mPublisherInterstitialAd.loadAd(publisherAdRequest);}*/

            //progDailog.dismiss();
            //finish();
            //if(progDailog.isShowing())
            //{
            //progDailog.dismiss();
            //}



            //super.onBackPressed();
        //}

    }}




