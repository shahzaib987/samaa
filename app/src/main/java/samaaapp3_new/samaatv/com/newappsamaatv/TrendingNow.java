package samaaapp3_new.samaatv.com.newappsamaatv;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


public class TrendingNow extends Fragment {
    String videourl;
    ImageView playbtn;
    CardView cardView;
    String t1, d1, image_trend;
    String ticker_main;
    JSONArray pakistan = null;
    JSONArray world= null;
    JSONArray editorchoice=null;
    JSONArray business = null;
    JSONArray entertainment = null;
    JSONArray sports = null;
    JSONArray ticker = null;
    JSONArray trending =null;

    SharedPreferences settings;
    String json;
    ArrayList<String> ticker_array = new ArrayList<String>();
   // FrameLayout ticker_layout;
    TextView pakistan_head1,pakistan_head2, global_head1, global_head2, editor_head1, editor_head2, sports_head1, sports_head2,
    enter_head1, enter_head2, bus_head1, bus_head2;

    TextView more_pak, more_glo, more_enter, more_bus, more_editor, more_sports;

    private PublisherAdView mAdView, mAdView1, mAdView2;


    RecyclerView pakistan_recycle, world_recycle, sports_recycle, edchoice_recycle, enter_recycle, business_recycle, ticker_recycle;
    private RecyclerView.Adapter pakistan_adapter, world_adapter, sports_adapter, edchoice_adapter, enter_adapter, business_adapter, ticker_adapter;
    public static final String PREFS_NAME = "Trending";
    TextView titletrend, datetrend, title1, date1;
    ImageView imagetrending;
    // URL to get contacts JSON
    String jsonURL = "http://www.samaa.tv/jappmain/";
    private String TAG = TrendingNow.class.getSimpleName();

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    private String tickerFullSize;
    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // flag for Internet connection Connection Fast status
    Boolean isConnectedFast = false;
   // SharedPreferences prefs1;
    SharedPreferences.Editor editor;

    // internet speed class class
    InternetSpeed is;

    ArrayList<HashMap<String, String>> contactList;

    public TrendingNow() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  prefs1=PreferenceManager.getDefaultSharedPreferences(getActivity());
        settings = getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        json = settings.getString("JSONString", null);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        // return inflater.inflate(R.layout.trend_layout, container, false);

        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.trend_layout, container, false);
        cardView = (CardView) rootView.findViewById(R.id.card_view1);
        pakistan_head1=(TextView)rootView.findViewById(R.id.pakistan_head1);
        pakistan_head2=(TextView)rootView.findViewById(R.id.pakistan_head2);

        global_head1=(TextView)rootView.findViewById(R.id.global_head1);
        global_head2=(TextView)rootView.findViewById(R.id.global_head2);

        editor_head1=(TextView)rootView.findViewById(R.id.editor_head1);
        editor_head2=(TextView)rootView.findViewById(R.id.editor_head2);

        sports_head1=(TextView)rootView.findViewById(R.id.sports_head1);
        sports_head2=(TextView)rootView.findViewById(R.id.sports_head2);


        enter_head1=(TextView)rootView.findViewById(R.id.enter_head1);
        enter_head2=(TextView)rootView.findViewById(R.id.enter_head2);

        bus_head1=(TextView)rootView.findViewById(R.id.bus_head1);
        bus_head2=(TextView)rootView.findViewById(R.id.bus_head2);

        //Applying two fonts for news headings
        Typeface face= Typeface.createFromAsset(getActivity().getAssets(), "font/Heading1.ttf");
        pakistan_head1.setTypeface(face);

        Typeface face1= Typeface.createFromAsset(getActivity().getAssets(), "font/Heading2.ttf");
        pakistan_head2.setTypeface(face1);

        Typeface face3= Typeface.createFromAsset(getActivity().getAssets(), "font/Heading1.ttf");
        global_head1.setTypeface(face3);

        Typeface face4= Typeface.createFromAsset(getActivity().getAssets(), "font/Heading2.ttf");
        global_head2.setTypeface(face4);

        Typeface face5= Typeface.createFromAsset(getActivity().getAssets(), "font/Heading1.ttf");
        editor_head1.setTypeface(face5);

        Typeface face6= Typeface.createFromAsset(getActivity().getAssets(), "font/Heading2.ttf");
        editor_head2.setTypeface(face6);


        Typeface face7= Typeface.createFromAsset(getActivity().getAssets(), "font/Heading1.ttf");
        sports_head1.setTypeface(face7);

        Typeface face8= Typeface.createFromAsset(getActivity().getAssets(), "font/Heading2.ttf");
        sports_head2.setTypeface(face8);

        Typeface face9= Typeface.createFromAsset(getActivity().getAssets(), "font/Heading1.ttf");
        enter_head1.setTypeface(face9);

        Typeface face10= Typeface.createFromAsset(getActivity().getAssets(), "font/Heading2.ttf");
        enter_head2.setTypeface(face10);


        Typeface face11= Typeface.createFromAsset(getActivity().getAssets(), "font/Heading1.ttf");
        bus_head1.setTypeface(face11);

        Typeface face12= Typeface.createFromAsset(getActivity().getAssets(), "font/Heading2.ttf");
        bus_head2.setTypeface(face12);

        playbtn = (ImageView) rootView.findViewById(R.id.play_vdo);

        mAdView = (PublisherAdView) rootView.findViewById(R.id.ad_view1);
        mAdView1 = (PublisherAdView) rootView.findViewById(R.id.ad_view2);
        mAdView2 = (PublisherAdView) rootView.findViewById(R.id.ad_view3);

      //  mAdView.setAdSizes(AdSize.MEDIUM_RECTANGLE);
       // mAdView.setAdUnitId(getString(R.string.medium_rect));

        // Create a banner ad. The ad size and ad unit ID must be set before calling loadAd.
       /* mAdView = new PublisherAdView(getActivity());
        mAdView1 = new PublisherAdView(getActivity());
        mAdView2 = new PublisherAdView(getActivity());

        mAdView.setAdSizes(AdSize.MEDIUM_RECTANGLE);
        mAdView1.setAdSizes(AdSize.MEDIUM_RECTANGLE);
        mAdView2.setAdSizes(AdSize.MEDIUM_RECTANGLE);

        mAdView.setAdUnitId(getString(R.string.medium_rect));
        mAdView1.setAdUnitId(getString(R.string.medium_rect));
        mAdView2.setAdUnitId(getString(R.string.medium_rect));*/

        PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();

        // Start loading the ad in the background.
        mAdView.loadAd(adRequest);
        mAdView1.loadAd(adRequest);
        mAdView2.loadAd(adRequest);


        more_pak = (TextView) rootView.findViewById(R.id.more_pak);
        more_glo = (TextView) rootView.findViewById(R.id.more_glo);

        more_enter = (TextView) rootView.findViewById(R.id.more_enter);
        more_editor = (TextView) rootView.findViewById(R.id.more_edit);

        more_bus = (TextView) rootView.findViewById(R.id.more_bus);
        more_sports = (TextView) rootView.findViewById(R.id.more_sport);

        more_pak.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                MainActivity.viewPager.setCurrentItem(2);
//                getActivity().getActionBar().setSelectedNavigationItem(2);
            }
        });

        more_glo.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                MainActivity.viewPager.setCurrentItem(3);
//                getActivity().getActionBar().setSelectedNavigationItem(2);
            }
        });

        more_enter.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                MainActivity.viewPager.setCurrentItem(6);
//                getActivity().getActionBar().setSelectedNavigationItem(2);
            }
        });

        more_editor.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                MainActivity.viewPager.setCurrentItem(7);
//                getActivity().getActionBar().setSelectedNavigationItem(2);
            }
        });

        more_bus.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                MainActivity.viewPager.setCurrentItem(4);
//                getActivity().getActionBar().setSelectedNavigationItem(2);
            }
        });

        more_sports.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                MainActivity.viewPager.setCurrentItem(5);
//                getActivity().getActionBar().setSelectedNavigationItem(2);
            }
        });

        titletrend = (TextView) rootView.findViewById(R.id.title_trend);

        datetrend = (TextView) rootView.findViewById(R.id.date_trend);

        imagetrending = (ImageView) rootView.findViewById(R.id.img_trend);

        pakistan_recycle = (RecyclerView) rootView.findViewById(R.id.list_pak);

        world_recycle = (RecyclerView) rootView.findViewById(R.id.list_glo);

        sports_recycle = (RecyclerView) rootView.findViewById(R.id.list_spo);

        enter_recycle = (RecyclerView) rootView.findViewById(R.id.list_enter);

        business_recycle = (RecyclerView) rootView.findViewById(R.id.list_bus);

        edchoice_recycle = (RecyclerView) rootView.findViewById(R.id.editor_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);

        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);

        LinearLayoutManager layoutManager3 = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);

        LinearLayoutManager layoutManager4 = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);

        LinearLayoutManager layoutManager5 = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);

        LinearLayoutManager layoutManager6 = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);

       //RecyclerView for vertical layout
        pakistan_recycle.setLayoutManager(layoutManager2);
        pakistan_recycle.setNestedScrollingEnabled(false);
        pakistan_recycle.setHasFixedSize(true);

        //world
        world_recycle.setLayoutManager(layoutManager3);
        world_recycle.setNestedScrollingEnabled(false);
        world_recycle.setHasFixedSize(true);

        //entertainment
        enter_recycle.setLayoutManager(layoutManager4);
        enter_recycle.setNestedScrollingEnabled(false);
        enter_recycle.setHasFixedSize(true);

        //business
        business_recycle.setLayoutManager(layoutManager5);
        business_recycle.setNestedScrollingEnabled(false);
        business_recycle.setHasFixedSize(true);

        //sports
        sports_recycle.setLayoutManager(layoutManager6);
        sports_recycle.setNestedScrollingEnabled(false);
        sports_recycle.setHasFixedSize(true);

        //RecyclerView for horizontal layout
        edchoice_recycle.setLayoutManager(layoutManager);
        edchoice_recycle.setHasFixedSize(true);


        isConnectedFast = is.isConnectedFast(getActivity());

        try {
            new GetContacts().execute();
        }
        catch(Exception e){
            Log.e(TAG, "GetContactAsyncError: " + e.getMessage());
        }

        //For making ticker visible..
      //  MainActivity.frameticker.setVisibility(View.VISIBLE);

        return rootView;
    }


    private class GetContacts extends AsyncTask<Void, Void, Void> {

        ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading SAMAATV News...");
            pDialog.setCancelable(false);
            pDialog.show();

        }

        @Override
        protected Void doInBackground(Void... arg0) {

            ServiceHandler sh = new ServiceHandler();


            String jsonStr = sh.makeServiceCall(jsonURL, ServiceHandler.GET);
            //String ti,da;

            Log.d("Response: ", "> " + jsonStr);


            JSONObject jsonObj = null;

            if (jsonStr != null) {
                try {
                    //JSONObject jsonObj = new JSONObject(jsonStr);

                   // JSONObject jsonObj = new JSONObject(jsonStr);

                  if (isConnectedFast) {
                      // Your Volley Code
                      // Store JSON
                      jsonObj = new JSONObject(jsonStr);
                      setUserObject(getActivity(), jsonObj.toString(),"json_response");
                     // Log.d("Json_Response: ", "> " + getUserObject(getActivity(),"json_response"));
                  }
                    else
                  {
                      //jsonObj = new JSONObject(jsonStr);
                      jsonObj = new JSONObject(getUserObject(getActivity(),"json_response"));
                  }



                    // Getting JSON Array node of categories of news
                    trending = jsonObj.getJSONArray("Trending_Now");
                    pakistan = jsonObj.getJSONArray("National");
                    sports = jsonObj.getJSONArray("Sports");
                    editorchoice = jsonObj.getJSONArray("Editors_Choice");
                    entertainment = jsonObj.getJSONArray("Entertainment");
                    business = jsonObj.getJSONArray("Economy");
                    world = jsonObj.getJSONArray("Global");
                    ticker = jsonObj.getJSONArray("Breaking");

                    tickerFullSize = "";
                    //Getting JSONArray of Ticker News to get all news items
                    for (int i = 0; i < ticker.length(); i++) {

                         JSONObject ticker_objects = ticker.getJSONObject(i);

                          // String id = trend.getString("id");
                         ticker_main = ticker_objects.getString("title");
                         ticker_array.add(ticker_main);
                        tickerFullSize += ticker_main;
                        Log.e("ticker main", ticker_main);
                    }
                    // }


                    //Getting JSONArray of Trending News to get all news items  - Done !
                   // for (int i = 0; i < trending.length(); i++) {
                   JSONObject trend = trending.getJSONObject(0);

                       // String id = trend.getString("id");
                        t1 = trend.getString("title");
                      //  String desc = trend.getString("desc");
                        //String link = trend.getString("link");
                        image_trend = trend.getString("image");
                        videourl = trend.getString("videourl");
                       // String videourl = trend.getString("videourl");
                        d1 = trend.getString("pubDate");
                       // String category = trend.getString("category");

                   // }

                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    Toast.makeText(getActivity(), "Json parsing error: " + e.getMessage(), Toast.LENGTH_LONG).show();

                }

              } else {
                Log.e("ServiceHandler", "URL'den veri alınamadı.");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            titletrend.setText(Html.fromHtml(t1));
            MainActivity.ticker_head.setText("");

            if(videourl!=null && !videourl.equals("None")) {
                playbtn.setVisibility(View.VISIBLE);
            }
            else
            {
                playbtn.setVisibility(View.GONE);
            }


            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //implement onClick
                    String jsonArray = trending.toString();
                    Intent intent = new Intent(getActivity(), Detail_Activity.class);
                    intent.putExtra("jsonArray", jsonArray);
                    intent.putExtra("pos", 0);
                    //intent.putExtra("category", cat);
				/*intent.putExtra("link",link);
				intent.putExtra("title",lfflTitle.getText());*/
                    //feed.getItem()
                    startActivity(intent);
                }
            });
            //Setting ticker starts here
            DisplayMetrics displaymetrics = new DisplayMetrics();
            ((Activity) getContext()).getWindowManager()
                    .getDefaultDisplay().getMetrics(displaymetrics);
            int screenWidth = displaymetrics.widthPixels;

            int textWidth = MainActivity.ticker_head.getMeasuredWidth();

            /*get correct length modifier*/
            float densityMultiplier = getContext().getResources().getDisplayMetrics().density;
            float scaledPx = 14 * densityMultiplier;
            Paint paint = new Paint();
            paint.setTypeface(Typeface.DEFAULT);
            paint.setTextSize(scaledPx);

           // paint.setTextSize(fontSize);
            float textWidth_actual = paint.measureText(tickerFullSize, 0, tickerFullSize.length());
            float textWidth_start = paint.measureText(ticker_main, 0, ticker_main.length());


            Animation animationToLeft = new TranslateAnimation(textWidth_start , -textWidth_actual, 0, 0);
            animationToLeft.setDuration(21000); //should i increase more or make it less?
            animationToLeft.setRepeatMode(Animation.RESTART);
            animationToLeft.setRepeatCount(Animation.INFINITE);

          /*  Animation animationToRight = new TranslateAnimation(-400,400, 0, 0);
            animationToRight.setDuration(12000);
            animationToRight.setRepeatMode(Animation.RESTART);
            animationToRight.setRepeatCount(Animation.INFINITE);*/

            MainActivity.ticker_head.setAnimation(animationToLeft);
            // textViewMarqToRight.setAnimation(animationToRight);
            for(int i = 0; i<ticker_array.size();i++){
            MainActivity.ticker_head.append(ticker_array.get(i));
            MainActivity.ticker_head.append(" | ");
            }

            //Setting ticker ends here

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
            Date testDate = null;
            try {
                testDate = sdf.parse(d1);
            }catch(Exception ex){
                ex.printStackTrace();
            }

            SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
            String newFormat = formatter.format(testDate);
            System.out.println(".....Date..." + newFormat);
            // end date format

            long milliseconds = testDate.getTime();

            //  long longDate = newFormat;
            // String result = DateUtils.getRelativeTimeSpanString(mContext, );
            //Setting text view date
            // date.setText(newFormat);

            String gettime = getTimeAgo(milliseconds);

            datetrend.setText(gettime);

           /* Glide.with(getActivity())
                    .load(image_trend)
                    .placeholder(R.drawable.cast_ic_notification_2)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .into(imagetrending);*/


            Glide.with(getActivity())
                    .load(image_trend)
                    .error(R.drawable.logo_samaatv)
                    .placeholder(R.drawable.logo_samaatv)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imagetrending);

            /*Picasso.with(getActivity())
                    .load(image_trend)
                    .placeholder(R.drawable.cast_ic_notification_2)
                    .fit()
                    .into(imagetrending);*/

            // Setting data for Pakistan News

            pakistan_adapter = new RecyclerViewAdapterCommon(getActivity(), pakistan);
            //mAdapter = new RecyclerViewMaterialAdapter();
            pakistan_recycle.setAdapter(pakistan_adapter);

            /*//Setting data for SAMAA TV News Ticker
            pakistan_adapter = new RecyclerViewAdapterCommon(getActivity(), pakistan);
            //mAdapter = new RecyclerViewMaterialAdapter();
            pakistan_recycle.setAdapter(pakistan_adapter);*/
           // pakistan_adapter.notifyDataSetChanged();

            //Ending pakistan data

            // Setting data for World News

            world_adapter = new RecyclerViewAdapterCommon(getActivity(), world);
            //mAdapter = new RecyclerViewMaterialAdapter();
            world_recycle.setAdapter(world_adapter);
            //world_adapter.notifyDataSetChanged();

            //Ending world data

            // Setting data for Sports News

            sports_adapter = new RecyclerViewAdapterCommon(getActivity(), sports);
            //mAdapter = new RecyclerViewMaterialAdapter();
            sports_recycle.setAdapter(sports_adapter);
           // sports_adapter.notifyDataSetChanged();

            //Ending sports data

            // Setting data for Business News

            business_adapter = new RecyclerViewAdapterCommon(getActivity(), business);
            //mAdapter = new RecyclerViewMaterialAdapter();
            business_recycle.setAdapter(business_adapter);
            //business_adapter.notifyDataSetChanged();

            //Ending Business data

            // Setting data for Entertainment News

            enter_adapter = new RecyclerViewAdapterCommon(getActivity(), entertainment);
            //mAdapter = new RecyclerViewMaterialAdapter();
            enter_recycle.setAdapter(enter_adapter);
            //enter_adapter.notifyDataSetChanged();

            //Ending Entertainment data

            // Setting data for Editors Choice News --  Horizontal View

            edchoice_adapter = new RecyclerViewEditorAdapter(getActivity(), editorchoice);
            //mAdapter = new RecyclerViewMaterialAdapter();
            edchoice_recycle.setAdapter(edchoice_adapter);
           // edchoice_adapter.notifyDataSetChanged();

            //Ending Editors Choice data

            // Setting data for Ticker News

           // ticker_adapter = new RecyclerViewAdapterCommon(getActivity(), ticker);
            //mAdapter = new RecyclerViewMaterialAdapter();
            //ticker_recycle.setAdapter(ticker_adapter);
            //enter_adapter.notifyDataSetChanged();

            //Ending Ticker data

            if (null != pDialog && pDialog.isShowing()) {
                pDialog.dismiss();}

        }

    }

    public static void setUserObject(Context c, String userObject,String key) {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(c);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, userObject);
        editor.commit();
    }

    public static String getUserObject(Context ctx,String key) {
        SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        String userObject = pref.getString(key, null);
        return userObject;
    }

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "Just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null && mAdView1 != null && mAdView2 != null) {
            mAdView.resume();
            mAdView1.resume();
            mAdView2.resume();
        }
    }


    @Override
    public void onPause() {
        // Pause the PublisherAdView.
        if (mAdView != null && mAdView1 != null && mAdView2 != null){
            mAdView.pause();
            mAdView1.pause();
            mAdView2.pause();
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        // Destroy the PublisherAdView.
        if (mAdView != null && mAdView1 != null && mAdView2 != null) {
            mAdView.destroy();
            mAdView1.destroy();
            mAdView2.destroy();
        }
        super.onDestroy();
    }

}