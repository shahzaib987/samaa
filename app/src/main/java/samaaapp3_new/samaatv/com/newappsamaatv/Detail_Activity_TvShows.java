package samaaapp3_new.samaatv.com.newappsamaatv;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import me.relex.circleindicator.CircleIndicator;


public class Detail_Activity_TvShows extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    static PublisherInterstitialAd mPublisherInterstitialAd;

    SwipeRefreshLayout swipeLayout;
    RecyclerView.LayoutManager layoutManager;
    static final boolean GRID_LAYOUT = false;
    private RecyclerView mRecyclerView;
    static FrameLayout frame_footer;
    JSONArray programsList=null;
    private RecyclerView.Adapter mAdapter;
    private String TAG = TVShowListing_Activity.class.getSimpleName();
    String feedURL;
    String url;
    int pos;
    //private PublisherAdView mAdView;
    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */


    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_recyclerview);


        //   getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getActionBar().setHomeButtonEnabled(true);
        //setTitle("My new title");

        //getSupportActionBar().setDisplayShowTitleEnabled(false);
        // getSupportActionBar().setDisplayShowHomeEnabled(true);
        // ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER;
        // getSupportActionBar().setDisplayUseLogoEnabled(true);
        //   getSupportActionBar().setLogo(R.drawable.samaa_logo);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(R.layout.app_bar_detail);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.toolbar_layoutcolor)));


        // getSupportActionBar().setDisplayUseLogoEnabled(true);
        //   getSupportActionBar().setB
     /*   Bundle args = getIntent().getExtras();
        if (args  != null )
        {
            url = args.getString("url");
            pos = args.getInt("item_selected_key");

        }*/

        Intent intent = getIntent();
        if (intent != null){
            url = intent.getStringExtra("url");
            pos = intent.getExtras().getInt("item_selected_key");
            feedURL = url;
        }

        new AsyncLoadXMLFeed().execute();
        //String pos=getArguments().getString("pos");
//        url= this.getIntent().getExtras().getString("url");

        // Inflate the layout for this fragment
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        //mAdView = (PublisherAdView) findViewById(R.id.ad_view);


        //PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();

        // Start loading the ad in the background.
        //mAdView.loadAd(adRequest);
        // mAdView = (PublisherAdView) view.findViewById(R.id.ad);
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeLayout.setOnRefreshListener(this);




        swipeLayout.setColorSchemeColors(R.color.toolbar_layoutcolor,
                R.color.heading1_black,
                R.color.heading2_black,
                R.color.blue);


        frame_footer = (FrameLayout) findViewById(R.id.footer);






        if (getApplicationContext().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            layoutManager = new GridLayoutManager(getApplicationContext(), 2);
        } else {
            layoutManager = new LinearLayoutManager(getApplicationContext());
        }
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);



        //  indicator.setViewPager(mPager);
        //  mPagerAdapter.registerDataSetObserver(indicator.getDataSetObserver());
    }

    private class AsyncLoadXMLFeed extends AsyncTask<Void, Void, Void> {

        //ProgressDialog pDialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

          /*  pDialog = new ProgressDialog(getApplicationContext());
            pDialog.setMessage("Loading Episodes...");
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();*/

        }

        @Override
        protected Void doInBackground(Void... params) {

            ServiceHandler sh = new ServiceHandler();

            String jsonStr = sh.makeServiceCall(feedURL, ServiceHandler.GET);


            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {

                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node of TV Shows News
                    programsList = jsonObj.getJSONArray("Program-Episodes");



                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Json parsing error: " + e.getMessage(), Toast.LENGTH_LONG).show();

                }
            } else {
                Log.e("ServiceHandler", "URL'den veri alınamadı.");
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);



           /* if (null != pDialog && pDialog.isShowing()) {
                pDialog.dismiss();*/

                mAdapter = new RecyclerViewAdapterProgramsListingAds(getApplicationContext(), programsList);
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            //}

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        finish();
        /*if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }*/
    }

    public void onRefresh() {
        // TODO Auto-generated method stub
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                mAdapter = new RecyclerViewAdapterProgramsListingAds(getApplicationContext(), programsList);
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                swipeLayout.setRefreshing(false);
            }
        }, 5000);
       /* new AsyncLoadXMLFeed().execute();
        swipeLayout.setRefreshing(false);*/
    }}


/**
 * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
 * sequence.
 */

