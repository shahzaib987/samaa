package samaaapp3_new.samaatv.com.newappsamaatv;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

//import com.github.florent37.materialviewpager.sample.fragment.Detail_Activity;


public class RecyclerViewAdapterCommon extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    JSONArray contents;
    private String TAG = RecyclerViewAdapterCommon.class.getSimpleName();

    //private List<RSSItem> listItems, filterList;

    // private List<RSSItem> _itemlist;

    protected ImageView imageView, playbtn;
    protected TextView title, date, category;
    private Context mContext;

    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    static final int TYPE_HEADER = 0;
    static final int TYPE_CELL = 1;

    public RecyclerViewAdapterCommon(Context context, JSONArray contents) {
        this.contents = contents;
        this.mContext = context;
        //  this.listItems = new ArrayList<RSSItem>();

    }


    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return TYPE_HEADER;
            default:
                return TYPE_CELL;
        }
    }

    @Override
    public int getItemCount() {
        //return contents.getItemCount();

        return 4;
    }

 /*   @Override
    public int getViewTypeCount() {
        return 2;
    }
*/

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        /*final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_card_big, parent, false);*/


        if (viewType == TYPE_HEADER) {
            View headerView = inflater.inflate(R.layout.list_item_card_big, parent, false);
            return new RecyclerView.ViewHolder(headerView) {
            }; // view holder for header items
        } else if (viewType == TYPE_CELL) {
            View normalRow = inflater.inflate(R.layout.items_new, parent, false);
            return new RecyclerView.ViewHolder(normalRow) {
            }; // view holder for normal items
        }
        else
        {
            View view = inflater.inflate(R.layout.list_item_card_big, parent, false);
            return new RecyclerView.ViewHolder(view){

            };
        }
        //return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        imageView = (ImageView) holder.itemView.findViewById(R.id.img);
        playbtn = (ImageView) holder.itemView.findViewById(R.id.playicon);
        title = (TextView) holder.itemView.findViewById(R.id.title);
        date = (TextView) holder.itemView.findViewById(R.id.date);
        category = (TextView) holder.itemView.findViewById(R.id.cate);

            final int posAcu = holder.getAdapterPosition();

        try {
                JSONObject pak = contents.getJSONObject(position);

                String id = pak.getString("id");
                String title1 = pak.getString("title");
                String desc = pak.getString("desc");
                String link = pak.getString("link");
                String image = pak.getString("image");

                String video = pak.getString("videourl");
                String pubDate = pak.getString("pubDate");
                String category1 = pak.getString("category");

                category.setText("| "+category1);
                title.setText(Html.fromHtml(title1));

            //setting text for ticker temporarily
           // MainActivity.ticker_head.setSelected(true);

          //  Point size = new Point();
            //getWindowManager().getDefaultDisplay().getSize(size);
          // int screenWidth = size.x;


            // Measure the size of textView
           // MainActivity.ticker_head.measure(0, 0);
           /* int textWidth = MainActivity.ticker_head.getMeasuredWidth();

            WindowManager wm = (WindowManager) mContext
                    .getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            int width = display.getWidth();

            Animation animationToLeft = new TranslateAnimation(width, -width, 0, 0);
            animationToLeft.setDuration(12000);
            animationToLeft.setRepeatMode(Animation.RESTART);
            animationToLeft.setRepeatCount(Animation.INFINITE);

            Animation animationToRight = new TranslateAnimation(-400,400, 0, 0);
            animationToRight.setDuration(12000);
            animationToRight.setRepeatMode(Animation.RESTART);
            animationToRight.setRepeatCount(Animation.INFINITE);

            MainActivity.ticker_head.setAnimation(animationToLeft);
           // textViewMarqToRight.setAnimation(animationToRight);
            MainActivity.ticker_head.setText(title1);*/
            //MainActivity.ticker_head.setAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_animation));


                // Setting date with formatted elapsed time
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
                Date testDate = null;
                try {
                    testDate = sdf.parse(pubDate);
                }catch(Exception ex){
                    ex.printStackTrace();
                }

                SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
                String newFormat = formatter.format(testDate);
                System.out.println(".....Date..." + newFormat);
                // end date format

                long milliseconds = testDate.getTime();

                //  long longDate = newFormat;
                // String result = DateUtils.getRelativeTimeSpanString(mContext, );
                //Setting text view date
                // date.setText(newFormat);

                String gettime = getTimeAgo(milliseconds);

                date.setText(gettime);

                //Download image using picasso library
               /* Picasso.with(mContext.getApplicationContext())
                        .load(image)
                        .fit()
                        .error(R.drawable.logo_samaatv)
                        .placeholder(R.drawable.logo_samaatv)
                        .into(imageView);*/

            Glide.with(mContext.getApplicationContext())
                    .load(image)
                    .error(R.drawable.logo_samaatv)
                    .placeholder(R.drawable.logo_samaatv)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);

            if(video!=null && !video.equals("None")) {
                playbtn.setVisibility(View.VISIBLE);
           }
            else
            {
                playbtn.setVisibility(View.GONE);
            }



        }catch(Exception e){
            Log.e(TAG, "Json parsing error: " + e.getMessage());
            //Toast.makeText(getActivity(), "Json parsing error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }


       // holder.title.
       // final int pos = holder.getAdapterPosition();


        //Setting text view title
        //title.setText(Html.fromHtml(contents.getItem(position).getTitle()));



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String jsonArray = contents.toString();


                // Bundle bundle = new Bundle();
                //  String json_array =bundle.getString("userdata");
                //Toast.makeText(mContext, "Recycle Click" + position + Html.fromHtml(contents.getItem(position).getTitle()), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(mContext, Detail_Activity.class);
                intent.putExtra("jsonArray", jsonArray);
                intent.putExtra("pos", position);
                //intent.putExtra("category", cat);
				/*intent.putExtra("link",link);
				intent.putExtra("title",lfflTitle.getText());*/
                //feed.getItem()
                mContext.startActivity(intent);

                //RecyclerView.ViewHolder holder1 = (RecyclerView.ViewHolder) view.getTag();
//                int position = holder1.getAdapterPosition();
              //  RSSItem feedItem = contents.getItem(pos);
              //  Bundle bundle = new Bundle();
              //  bundle.putSerializable("feed", contents);
                //Bundle bundle = new Bundle();
                //bundle.putSerializable("feed", contents);
               // Toast.makeText(mContext, "Recycle Click" + position + title.getText(), Toast.LENGTH_SHORT).show();
               // Intent intent = new Intent(mContext, Detail_Activity.class);
               // intent.putExtras(bundle);
                //intent.putExtra("pos", pos);
                //intent.putExtra("category", cat);
				/*intent.putExtra("link",link);
				intent.putExtra("title",lfflTitle.getText());*/
                //feed.getItem()
                //mContext.startActivity(intent);
            }
        });

        /*  switch (getItemViewType(position)) {
            case TYPE_HEADER:
                break;
            case TYPE_CELL:
                break;
        }*/
    }

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "Just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

   /* public void setItems(RSSFeed datas){
        contents = datas;
    }*/

  /*  public void setFilter(RSSFeed countryModels) {
        cno = new ArrayList<>();
        listItems.addAll(countryModels.getItemCount());
        notifyDataSetChanged();
    }
*/
    // Do Search...
 /* public void setFilter(List<RSSItem> countryModels) {
      listItems = new ArrayList<>();
      listItems.addAll(countryModels);
      notifyDataSetChanged();
  }*/
  /*  public static class ViewHolder extends RecyclerView.ViewHolder
            {
        private TextView titleTextView;

        public ViewHolder(View itemView) {
            super(itemView);

            titleTextView = (TextView)itemView.findViewById(R.id.textView);

        }


    }*/
}