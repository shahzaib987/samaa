package samaaapp3_new.samaatv.com.newappsamaatv;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DetailFragmentEditors extends Fragment {
    ProgressBar progress;

    JSONArray alsowatch=null;
    private RecyclerView.Adapter mAdapter_alsowatch;
    RecyclerView alsowatch_recycle;
    private String TAG = DetailFragmentEditors.class.getSimpleName();
    private PublisherAdView mAdView;
    private PublisherAdView mAdView1;
    private int fPos;
    String url;
    String arrayjson;
    String cat;
    TextView title, desc, desc2, date;
    ImageView image, playbtn;
    JSONArray array= null;
    int pos_detail;

    String t1, d1, desc1, videourl, link;
    static final String KEY_VIDEO = "videourl";
    String image_trend;
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);




        arrayjson = getArguments().getString("jsonArray");

        fPos = getArguments().getInt("pos");

        url = getArguments().getString("url");

    //   pos_detail = ((Detail_Activity_Editors) getActivity()).mPager.getCurrentItem();

        Log.d("test", "pos1 = " + fPos);

        Log.d("test2", "pos2 = " + pos_detail);



        try {
            array = new JSONArray(arrayjson);
          //  System.out.println(array.toString(2));
           // new AsyncLoadXMLFeed().execute();
        } catch (JSONException e) {
            e.printStackTrace();
        }



    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
        R.layout.detail_fragment_editors, container, false);

        progress = (ProgressBar) rootView.findViewById(R.id.progress2);
        title = (TextView) rootView.findViewById(R.id.title_detail);
        desc = (TextView) rootView.findViewById(R.id.desc_detail);
        desc2 = (TextView) rootView.findViewById(R.id.desc_detail2);
        date = (TextView) rootView.findViewById(R.id.date_detail);
        image = (ImageView) rootView.findViewById(R.id.img_detail);
        playbtn = (ImageView) rootView.findViewById(R.id.play_vdo);

        mAdView = (PublisherAdView) rootView.findViewById(R.id.ad_view1);
        mAdView1 = (PublisherAdView) rootView.findViewById(R.id.ad_view2);

        alsowatch_recycle = (RecyclerView) rootView.findViewById(R.id.alsowatch_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);

        //RecyclerView for horizontal layout
        alsowatch_recycle.setLayoutManager(layoutManager);
        alsowatch_recycle.setHasFixedSize(true);



        try{
                JSONObject obj = array.getJSONObject(fPos);
                String id = obj.getString("id");
                t1 = obj.getString("title");
                desc1 = obj.getString("desc");
                link = obj.getString("link");
                image_trend = obj.getString("image");

                videourl = obj.getString("videourl");
                d1 = obj.getString("pubDate");
                //String category = obj.getString("category");

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
                Date testDate = null;
                try {
                    testDate = sdf.parse(d1);
                }catch(Exception ex){
                    ex.printStackTrace();
                }
                SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
                String newFormat = formatter.format(testDate);
                System.out.println(".....Date..." + newFormat);


                long milliseconds = testDate.getTime();

                String gettime = getTimeAgo(milliseconds);

                date.setText(gettime);

                title.setText(t1);

                String video = videourl;

                if(video!=null){
                    playbtn.setVisibility(View.VISIBLE);

                }


         //   desc1 = desc1.replaceAll("<br>", "");

            //regular expression to remove image tag
            desc1 = desc1.replaceAll("<img.+?>", "");

            //regular expression to remove caption tag
            desc1 = desc1.replaceAll("(\\[caption(?:[^\\]]*)\\](?:.*?)\\[\\/caption\\])", "");


            //replace next line keywords to paragraph keyword
           // desc1 = desc1.replaceAll("\r","<p>");
          //  desc1 = desc1.replaceAll("\\r?\\n\\r?\\n","<p>");
            desc1 = desc1.replaceAll("\r\n","<br>");


            desc.setText(Html.fromHtml(desc1));
            new AsyncLoadXMLFeed().execute();
          /*  String[] separated = desc1.split("<p>");

            for(int i=0; i<3 ; i++){

                desc.setText(Html.fromHtml(separated[i]));

            }

            desc2.setText(Html.fromHtml(desc1.replaceAll(desc.getText().toString(), "")));*/
           // desc.setText(Html.fromHtml(separated[0]));
           // desc2.setText(Html.fromHtml(separated[1]));

           /* StringTokenizer tokens = new StringTokenizer(desc1, "<p>");
            String first = tokens.nextToken();// this will contain "Fruit"
            String second = tokens.nextToken();*/// this will contain " they taste good"
            // in the case above I assumed the string has always that syntax (foo: bar)
            // but you may want to check if there are tokens or not using the hasMoreTokens method

              /*  desc.setText(first);
                desc2.setText((second));*/

/*
                String[] separated = desc1.split("<p>");
                desc.setText(separated[0]);
                desc2.setText(separated[1].trim());

                Log.i("Desc 1", separated[0]);
                Log.i("Desc 2", separated[1]);*/
            //}
            //else
            //{
                //desc.setText(Html.fromHtml(desc1));
            //}

            //String[] separated = desc1.split("\\r?\\n");
         //   desc1 = desc1.trim();

         //   if(desc1.contains("<p>"))
            //desc.setText(separated[0]);
           // desc2.setText(separated[1]);
            //desc.setText(separated[0]); // this will contain "First Paragraph"
           // desc2.setText(separated[1]);
           // desc.setMovementMethod(LinkMovementMethod.getInstance());


           // desc.setText(Html.fromHtml(desc1)); //desc is showing from here
           // desc1.replace("<img.+?>", "");

            //desc.setMovementMethod(LinkMovementMethod.getInstance());

                /*Picasso.with(getActivity())
                        .load(image_trend)
                        .fit()
                        .error(R.drawable.samaa_logo)
                        .placeholder(R.drawable.samaa_logo)
                        .into(image);*/

            Glide.with(getActivity())
                    .load(image_trend)
                    .error(R.drawable.logo_samaatv)
                    .placeholder(R.drawable.logo_samaatv)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(image);


        }  catch (JSONException e) {
            e.printStackTrace();
        }

        if(link!=null){

            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().setContentUrl(link).build();

            // Start loading the ad in the background.
            mAdView.loadAd(adRequest);
            mAdView1.loadAd(adRequest);

        }
        else
        {


            PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();

            // Start loading the ad in the background.
            mAdView.loadAd(adRequest);
            mAdView1.loadAd(adRequest);

        }

        if(videourl!=null && !videourl.equals("None")) {
            playbtn.setVisibility(View.VISIBLE);
            playbtn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    // Starting new video
                    Intent in = new Intent(getActivity(), EditorVideoWeb.class);
                    //	in.putExtra(KEY_TITLE, name);
                    //	in.putExtra(KEY_ARTIST, cost);
                    in.putExtra(KEY_VIDEO, videourl);
                    in.putExtra("image", image_trend);
                    startActivity(in);
                }
            });
        }
        else
        {
            playbtn.setVisibility(View.GONE);
        }
        return rootView;
    }

    private class AsyncLoadXMLFeed extends AsyncTask<Void, Void, Void> {

      //  ProgressDialog pDialog;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress.setVisibility(View.VISIBLE);
         /*   pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
*/
        }

        @Override
        protected Void doInBackground(Void... params) {

            ServiceHandler sh = new ServiceHandler();

            String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);


            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                try {

                    JSONObject jsonObj = new JSONObject(jsonStr);

                    // Getting JSON Array node of Editors Choice News
                    alsowatch = jsonObj.getJSONArray("Latest-Programs");



                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    Toast.makeText(getActivity(), "Json parsing error: " + e.getMessage(), Toast.LENGTH_LONG).show();

                }
            } else {
                Log.e("ServiceHandler", "URL'den veri alınamadı.");
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            progress.setVisibility(View.GONE);

          /*  if (null != pDialog && pDialog.isShowing()) {
                pDialog.dismiss();
*/
                // Setting data for Also Watch News --  Horizontal View
                mAdapter_alsowatch = new RecyclerViewEditorAdapter(getActivity(), alsowatch);
                alsowatch_recycle.setAdapter(mAdapter_alsowatch);
                mAdapter_alsowatch.notifyDataSetChanged();
       //     }

        }

    }

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            // if timestamp given in seconds, convert to millis
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "Just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
    }

}
