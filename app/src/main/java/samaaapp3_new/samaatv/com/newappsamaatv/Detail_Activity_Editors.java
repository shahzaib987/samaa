package samaaapp3_new.samaatv.com.newappsamaatv;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;

import org.json.JSONArray;
import org.json.JSONException;

//import me.relex.circleindicator.CircleIndicator;


public class Detail_Activity_Editors extends AppCompatActivity {
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
   // private static final int NUM_PAGES = 5;

    static PublisherInterstitialAd mPublisherInterstitialAd;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    public ViewPager mPager;
    JSONArray array;
    String jsonArray;
    int pos;
    String url;
    String cate;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);


     //   getSupportActionBar().setDisplayShowTitleEnabled(false);
        //getActionBar().setHomeButtonEnabled(true);
        //setTitle("My new title");

        //getSupportActionBar().setDisplayShowTitleEnabled(false);
       // getSupportActionBar().setDisplayShowHomeEnabled(true);
       // ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER;
       // getSupportActionBar().setDisplayUseLogoEnabled(true);
     //   getSupportActionBar().setLogo(R.drawable.samaa_logo);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(R.layout.app_bar_detail);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(this, R.color.toolbar_layoutcolor)));


       // getSupportActionBar().setDisplayUseLogoEnabled(true);
     //   getSupportActionBar().setB


        // Create the InterstitialAd and set the adUnitId.
        mPublisherInterstitialAd = new PublisherInterstitialAd(this);
        // Defined in res/values/strings.xml
        mPublisherInterstitialAd.setAdUnitId(getString(R.string.interstitial_unit_id));

        PublisherAdRequest.Builder publisherAdRequestBuilder = new PublisherAdRequest.Builder();
        mPublisherInterstitialAd.loadAd(publisherAdRequestBuilder.build());
        //setTitle("Detail News");
        Intent intent = getIntent();
        jsonArray = intent.getStringExtra("jsonArray");
        try {
            array = new JSONArray(jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        pos = intent.getExtras().getInt("pos");

        url = intent.getExtras().getString("url");
        Log.e("","Position Activity is :"+pos);

       //feed = (RSSFeed) getIntent().getExtras().get("feed");

       // cate= getIntent().getExtras().getString("category");


        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
       // CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);

        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(pos);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

           if((position % 4) == 0 && mPublisherInterstitialAd != null && mPublisherInterstitialAd.isLoaded())

             {
                 mPublisherInterstitialAd.show();
             }


            }

            @Override
            public void onPageSelected(int position) {
                Log.d("test", "position = " + position);
                pos=position;
                Log.d("test2", "position2 = " + pos);
              //  pos = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
      //  indicator.setViewPager(mPager);
      //  mPagerAdapter.registerDataSetObserver(indicator.getDataSetObserver());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
      /*  int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);*/

        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        finish();
        /*if (mPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }*/
    }



    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class MyPagerAdapter extends FragmentStatePagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }



        @Override
        public Fragment getItem(int position) {

            DetailFragmentEditors frag = new DetailFragmentEditors();
            //pos = position;
            Bundle bundle = new Bundle();
            bundle.putString("jsonArray", jsonArray);
            bundle.putInt("pos", position);
            bundle.putString("url", url);
          //  bundle.putString("category", cate);

            //frag.set(position, fragment);
            //bundle.putString(KEY_VIDEO, videourl);
            frag.setArguments(bundle);

            return frag;

        }

        @Override
        public int getCount() {
            return array.length();
        }
    }
}